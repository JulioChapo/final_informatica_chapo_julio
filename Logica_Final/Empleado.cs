﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    class Empleado: Persona
    {
        public DateTime FechaDeNacimiento { get; set; }
        public Turnos Turno { get; set; }
        public Areas Area { get; set; }


        public enum Turnos
        {
            Mañana, Tarde
        }
        public enum Areas
        {
            Gerencia, Ventas, Instalacion
        }

        public override string DevolverDatos()
        {
            return base.DevolverDatos()+ $"-Turno: {Turno} -Area: {Area}";
        }

        public override string DevolverFiesta()
        {
            if (FechaDeNacimiento.Month== DateTime.Today.Month && FechaDeNacimiento.Day == DateTime.Today.Day)
            {
                return "¡Feliz cumpleaños compañero!";
            }
            return null;
        }
    }
}
