﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class Localidad
    {
        public string NombreCiudad { get; set; }
        public string Provincia { get; set; }
        public int CodigoPostal { get; set; }
    }
}
