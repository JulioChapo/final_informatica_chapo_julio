﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    class PiletaInflable: Pileta
    {
        public decimal Diametro { get; set; }
        public decimal Profundidad { get; set; }
        public bool CubrePiletas { get; set; }

        public override decimal CalcularPorcentajeDescuento()
        {
            return 0;
        }
    }
}
