﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    class Cliente: Persona
    {
        public decimal CodigoPiletaCliente { get; set; }
        public DateTime FechaInstalacion { get; set; }

        public override string DevolverFiesta()
        {
            if (FechaInstalacion.Month == DateTime.Today.Month && FechaInstalacion.Day == DateTime.Today.Day)
            {
                return "¡Felicitaciones por su primer aniversario de instalacion!";
            }
            return null;
        }
    }
}
